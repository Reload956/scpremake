<?php


namespace App\Tests\Utils;

use PHPUnit\Exception;
use PHPUnit\Framework\TestCase;
use App\Utils\Validator;

class ValidatorTest extends TestCase
{
public function testPasswordlongerzero(){
        $Validator = new Validator();
        $this->expectExceptionMessage("The password can not be empty.");
        $Validator->validatePassword("");
}
public function testPasswordTooShort(){
        $Validator = new Validator();
        $this->expectExceptionMessage("The password must be at least 6 characters long.");
        $Validator->validatePassword("asdad");
}
}

